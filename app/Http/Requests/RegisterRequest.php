<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // lỗi qua api
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:24|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|max:32|confirmed',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên',
            'name.min' => 'Tên phải có ít nhất 2 ký tự ',
            'name.max' => 'Tên không được vượt quá 12 ký tự ',
            'name.unique' => 'Tên này đã tồn tại ',
            'email.required' => 'Vui lòng nhập email ',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'Email này đã tồn tại',
            'password.required' => 'Vui lòng nhập mật khẩu ',
            'password.min' => 'Mật khẩu phải ít nhất 6 ký tự ',
            'password.max' => 'Mật khẩu không được vượt quá 32 ký tự ',
            'password.confirmed' => 'Mật khẩu không trùng khớp ',
        ];
    }
}
