window._ = require('lodash');

window.axios = require('axios');


window.axios.defaults.baseURL = 'http://' + window.location.host + '/api';
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.post['Accept'] = 'application/json'
window.axios.defaults.headers.put['Accept'] = 'application/json'
window.axios.defaults.headers.common = {'Authorization': `bearer ${localStorage.getItem('token')}`};
